package com.luo.demo.multi.ds.dynamic.controller;

import com.luo.demo.multi.ds.dynamic.base.BaseTest;
import com.luo.demo.multi.ds.dynamic.tenant.MyTenantLineHandlerImpl;
import org.junit.jupiter.api.Test;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * MyDataController测试
 *
 * @author luohq
 * @date 2022-08-06 15:43
 */
public class MyDataControllerTest extends BaseTest {

    @Test
    void findByIdFromDs1() throws Exception{
        Long id = 1L;
        this.mockMvc.perform(get("/myData/ds1/{id}", id)
                        .header(MyTenantLineHandlerImpl.TENANT_ID_HEADER, 1L))
                //打印请求详细信息
                .andDo(print())
                //判断返回结果200
                .andExpect(status().isOk())
                //判断content-type兼容application/json
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                //判断响应体respCode为成功
                .andExpect(jsonPath("$.id").value(id))
                .andExpect(jsonPath("$.myName").value("luo"));
    }

    @Test
    void findByIdFromDs2() throws Exception{
        Long id = 1L;
        this.mockMvc.perform(get("/myData/ds2/{id}", id)
                        .header(MyTenantLineHandlerImpl.TENANT_ID_HEADER, 2L))
                //打印请求详细信息
                .andDo(print())
                //判断返回结果200
                .andExpect(status().isOk())
                //判断content-type兼容application/json
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                //判断响应体respCode为成功
                .andExpect(jsonPath("$.id").value(id))
                .andExpect(jsonPath("$.myName").value("luo-2"));
    }
}
