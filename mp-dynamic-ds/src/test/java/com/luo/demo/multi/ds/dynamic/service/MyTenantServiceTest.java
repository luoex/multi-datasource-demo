package com.luo.demo.multi.ds.dynamic.service;

import com.luo.demo.multi.ds.dynamic.base.BaseTest;
import com.luo.demo.multi.ds.dynamic.entity.MyTenant;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;

/**
 * MyTenantService测试
 *
 * @author luohq
 * @date 2022-08-07 08:02
 */
@Slf4j
public class MyTenantServiceTest extends BaseTest {

    @Resource
    private IMyTenantService myTenantService;

    @Test
    void findById() {
        Long id = 1L;
        MyTenant myTenant = this.myTenantService.getById(id);
        log.info("getById result: {}", myTenant);
        Assertions.assertNotNull(myTenant);
        Assertions.assertEquals(id, myTenant.getId());
        Assertions.assertEquals("租户1", myTenant.getTenantName());
    }
}
