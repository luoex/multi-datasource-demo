package com.luo.demo.multi.ds.dynamic.service;

import com.luo.demo.multi.ds.dynamic.base.BaseTest;
import com.luo.demo.multi.ds.dynamic.dto.MyDataQueryDto;
import com.luo.demo.multi.ds.dynamic.entity.MyData;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.time.LocalDateTime;

/**
 * MyDataService测试
 *
 * @author luohq
 * @date 2022-08-07 08:02
 */
@Slf4j
public class MyDataServiceTest extends BaseTest {

    @Resource
    private IMyDataService myDataService;

    @Test
    void findByIdFromDs1() {
        Long id = 1L;
        String myName = "luo";
        MyData myData = this.myDataService.findByIdFromDs1(id);
        log.info("findByIdFromDs1 result: {}", myData);
        Assertions.assertNotNull(myData);
        Assertions.assertEquals(myName, myData.getMyName());
    }

    @Test
    void findByIdFromDs2() {
        Long id = 1L;
        String myName = "luo-2";
        MyData myData = this.myDataService.findByIdFromDs2(id);
        log.info("findByIdFromDs2 result: {}", myData);
        Assertions.assertNotNull(myData);
        Assertions.assertEquals(myName, myData.getMyName());
    }


    @Test
    void findByQueryFromDs1() {
        Long id = 1L;
        String myName = "luo";
        MyDataQueryDto myDataQueryDto = MyDataQueryDto.builder()
                .id(id)
                .myName(myName)
                .build();
        MyData myData = this.myDataService.findByQueryFromDs1(myDataQueryDto);
        log.info("findByQueryFromDs1 result: {}", myData);
    }

    @Test
    void findByName() {
        String myName = "luo";
        MyData myData = this.myDataService.findByName(myName);
        log.info("findByName result: {}", myData);
    }

    @Test
    void addData1() {
        MyData myData = this.buildMyData("luoNew-1");
        Integer retCount = this.myDataService.addData1(myData);
        log.info("addData1 retCount: {}", retCount);
    }

    @Test
    void addData2() {
        MyData myData = this.buildMyData( "luoNew-2");
        Integer retCount = this.myDataService.addData2(myData);
        log.info("addData2 retCount: {}", retCount);
    }

    @Test
    void addBothData() {
        MyData myData1 = this.buildMyData("luoNew-1");
        MyData myData2 = this.buildMyData("luoNew-2");
        Integer retCount = this.myDataService.addBothData(myData1, myData2);
        log.info("addBothData retCount: {}", retCount);
    }

    private MyData buildMyData(String myName) {
        MyData myData = new MyData();
        myData.setMyName(myName);
        myData.setMyType((byte) 2);
        //myData.setMyVersion(0);
        //myData.setTenantId(1L);
        myData.setCreatedBy("luo");
        myData.setCreatedTime(LocalDateTime.now());
        myData.setModifiedBy(myData.getCreatedBy());
        myData.setModifiedTime(myData.getCreatedTime());
        return myData;
    }
}
