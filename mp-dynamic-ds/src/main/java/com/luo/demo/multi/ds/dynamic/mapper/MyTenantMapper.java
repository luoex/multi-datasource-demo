package com.luo.demo.multi.ds.dynamic.mapper;

import com.luo.demo.multi.ds.dynamic.entity.MyTenant;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 我的租户 Mapper 接口
 * </p>
 *
 * @author luohq
 * @since 2022-08-07
 */
public interface MyTenantMapper extends BaseMapper<MyTenant> {

}
