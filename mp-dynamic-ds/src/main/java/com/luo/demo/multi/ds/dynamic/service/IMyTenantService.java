package com.luo.demo.multi.ds.dynamic.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.luo.demo.multi.ds.dynamic.entity.MyTenant;

/**
 * <p>
 * 我的租户 服务类
 * </p>
 *
 * @author luohq
 * @since 2022-08-07
 */
public interface IMyTenantService extends IService<MyTenant> {

}
