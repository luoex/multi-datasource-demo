package com.luo.demo.multi.ds.dynamic;

import com.luo.demo.multi.ds.dynamic.mapper.MyDataMapper;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 *
 * @author luohq
 * @date 2022-08-06
 */
@SpringBootApplication
@MapperScan(basePackageClasses = {MyDataMapper.class})
public class DynamicDsApplication {

	public static void main(String[] args) {
		SpringApplication.run(DynamicDsApplication.class, args);
	}

}
