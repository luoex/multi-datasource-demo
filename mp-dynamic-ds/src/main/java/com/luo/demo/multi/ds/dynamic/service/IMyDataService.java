package com.luo.demo.multi.ds.dynamic.service;

import com.luo.demo.multi.ds.dynamic.dto.MyDataQueryDto;
import com.luo.demo.multi.ds.dynamic.entity.MyData;

/**
 * <p>
 * 我的数据 服务类
 * </p>
 *
 * @author luohq
 * @since 2022-08-07
 */
public interface IMyDataService {
    MyData findByIdFromDs1(Long id);
    MyData findByQueryFromDs1(MyDataQueryDto myDataQueryDto);
    MyData findByName(String myName);
    MyData findByIdFromDs2(Long id);
    Integer addBothData(MyData myData1, MyData myData2);
    Integer addData1(MyData myData);
    Integer addData2(MyData myData);
}
