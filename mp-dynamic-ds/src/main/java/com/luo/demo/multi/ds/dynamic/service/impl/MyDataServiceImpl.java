package com.luo.demo.multi.ds.dynamic.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.luo.demo.multi.ds.dynamic.dto.MyDataQueryDto;
import com.luo.demo.multi.ds.dynamic.entity.MyData;
import com.luo.demo.multi.ds.dynamic.mapper.MyDataMapper;
import com.luo.demo.multi.ds.dynamic.service.IMyDataService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.Objects;

/**
 * <p>
 * 我的数据 服务实现类
 * </p>
 *
 * @author luohq
 * @since 2022-08-07
 */
@Service
public class MyDataServiceImpl implements IMyDataService {
    @Resource
    private MyDataMapper myDataMapper;

    @Override
    @DS("ds1")
    public MyData findByIdFromDs1(Long id) {
        //BaseMapper.selectById - 支持自动拼接租户Id参数
        //select .. where ... and tenant_id = ?
        return this.myDataMapper.selectById(id);
    }

    @Override
    @DS("ds1")
    public MyData findByQueryFromDs1(MyDataQueryDto myDataQueryDto) {
        //QueryWrapper - 支持自动拼接租户Id参数
        //select .. where ... and tenant_id = ?
        return this.myDataMapper.selectOne(Wrappers.<MyData>lambdaQuery()
                .eq(Objects.nonNull(myDataQueryDto.getId()), MyData::getId, myDataQueryDto.getId())
                .like(StringUtils.hasText(myDataQueryDto.getMyName()), MyData::getMyName, myDataQueryDto.getMyName()));
    }

    @Override
    public MyData findByName(String myName) {
        //mapper.xml自定义查询 - 支持自动拼接租户Id参数
        //select .. where name like '%...%' and tenant_id = ?
        return this.myDataMapper.selectByName(myName);
    }

    @Override
    @DS("ds2")
    public MyData findByIdFromDs2(Long id) {
        return this.myDataMapper.selectById(id);
    }

    /**
     * 单@Transactional内不支持切换数据源，
     * 即先使用ds1，则后续一直使用同一ds1连接，
     * 当前事务生效，但都会插入ds1中
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    public Integer addBothData(MyData myData1, MyData myData2) {
        Integer retCount1 = this.addData1(myData1);
        Integer retCount2 = this.addData2(myData2);
        //if (true) {
        //    throw new RuntimeException("业务异常 - 制造数据库回滚！");
        //}
        return retCount1 + retCount2;
    }

    /**
     * 支持事务
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    @DS("ds1")
    public Integer addData1(MyData myData) {
        //BaseMapper.insert - 支持自动设置tenantId
        //myData.tenantId可无需设置，由多租户插件负责自动填充tenant_id值
        //insert into my_data(... , tenant_id) values(... , ?)
        Integer retCount = this.myDataMapper.insert(myData);
        return retCount;
    }

    /**
     * 支持事务
     */
    @Override
    @Transactional(rollbackFor = Exception.class)
    @DS("ds2")
    public Integer addData2(MyData myData) {
        //支持自动设置tenantId
        Integer retCount = this.myDataMapper.insert(myData);
        return retCount;
    }
}
