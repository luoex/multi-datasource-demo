package com.luo.demo.multi.ds.dynamic.mapper;

import com.luo.demo.multi.ds.dynamic.entity.MyData;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 我的数据 Mapper 接口
 * </p>
 *
 * @author luohq
 * @since 2022-08-07
 */
public interface MyDataMapper extends BaseMapper<MyData> {

    MyData selectByName(String myName);
}
