package com.luo.demo.multi.ds.dynamic.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.luo.demo.multi.ds.dynamic.entity.MyTenant;
import com.luo.demo.multi.ds.dynamic.mapper.MyTenantMapper;
import com.luo.demo.multi.ds.dynamic.service.IMyTenantService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 我的租户 服务实现类
 * </p>
 *
 * @author luohq
 * @since 2022-08-07
 */
@Service
public class MyTenantServiceImpl extends ServiceImpl<MyTenantMapper, MyTenant> implements IMyTenantService {

}