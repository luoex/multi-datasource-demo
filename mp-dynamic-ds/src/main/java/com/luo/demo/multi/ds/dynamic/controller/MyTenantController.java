package com.luo.demo.multi.ds.dynamic.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 我的租户 前端控制器
 * </p>
 *
 * @author luohq
 * @since 2022-08-07
 */
@RestController
@RequestMapping("/myTenant")
public class MyTenantController {

}
