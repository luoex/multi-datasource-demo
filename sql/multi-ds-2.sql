/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 50725
 Source Host           : localhost:3306
 Source Schema         : multi-ds-2

 Target Server Type    : MySQL
 Target Server Version : 50725
 File Encoding         : 65001

 Date: 09/08/2022 16:14:49
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for my_data
-- ----------------------------
DROP TABLE IF EXISTS `my_data`;
CREATE TABLE `my_data`  (
  `id` bigint(20) NOT NULL COMMENT '主键ID',
  `my_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '名称',
  `my_type` tinyint(4) NOT NULL COMMENT '类型',
  `my_version` int(4) NOT NULL DEFAULT 0 COMMENT '版本号',
  `tenant_id` bigint(20) NOT NULL COMMENT '租户ID',
  `created_time` datetime(0) NOT NULL COMMENT '创建时间',
  `created_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '创建人',
  `modified_time` datetime(0) NOT NULL COMMENT '修改时间',
  `modified_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '我的数据' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of my_data
-- ----------------------------
INSERT INTO `my_data` VALUES (1, 'luo-2', 2, 0, 1, '2022-07-13 19:40:56', 'luo', '2022-08-06 15:37:34', 'luo');
INSERT INTO `my_data` VALUES (2, 'liuy-2', 3, 0, 1, '2022-07-13 19:42:13', 'luo', '2022-08-06 15:37:37', 'luo');

-- ----------------------------
-- Table structure for my_tenant
-- ----------------------------
DROP TABLE IF EXISTS `my_tenant`;
CREATE TABLE `my_tenant`  (
  `id` bigint(20) NOT NULL COMMENT '主键ID',
  `tenant_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '租户名称',
  `tenant_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '租户详情',
  `my_version` int(4) NOT NULL DEFAULT 0 COMMENT '版本号',
  `created_time` datetime(0) NOT NULL COMMENT '创建时间',
  `created_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '创建人',
  `modified_time` datetime(0) NOT NULL COMMENT '修改时间',
  `modified_by` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '修改人',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '我的租户' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of my_tenant
-- ----------------------------
INSERT INTO `my_tenant` VALUES (1, '租户1-2', '租户1说明', 0, '2022-08-06 10:36:31', 'luo', '2022-08-06 10:36:37', 'luo');
INSERT INTO `my_tenant` VALUES (2, '组合2-2', '租户2说明', 0, '2022-08-06 10:36:58', 'luo', '2022-08-06 10:37:04', 'luo');

SET FOREIGN_KEY_CHECKS = 1;
