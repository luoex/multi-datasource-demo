package com.luo.demo.multi.ds.pkg.service;

import cn.hutool.core.lang.Snowflake;
import cn.hutool.core.util.IdUtil;
import com.luo.demo.multi.ds.pkg.base.BaseTest;
import com.luo.demo.multi.ds.pkg.entity.MyData;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.time.LocalDateTime;

/**
 * MyDataService测试
 *
 * @author luohq
 * @date 2022-08-07 08:02
 */
@Slf4j
public class MyDataServiceTest extends BaseTest {

    @Resource
    private IMyDataService myDataService;

    private Snowflake snowflake = IdUtil.getSnowflake(1, 1);

    @Test
    void addData1() {
        //@Primary主数据源支持事务
        MyData myData = this.buildMyData(this.snowflake.nextId(), "luoNew-1");
        Integer retCount = this.myDataService.addData1(myData);
        log.info("addData1 retCount: {}", retCount);
    }

    @Test
    void addData2() {
        //非主数据源不支持事务
        MyData myData = this.buildMyData(this.snowflake.nextId(), "luoNew-2");
        Integer retCount = this.myDataService.addData2(myData);
        log.info("addData2 retCount: {}", retCount);
    }

    @Test
    void addBothData() {
        //仅@Primary主数据源ds1支持事务，非主数据源ds2不支持事务
        MyData myData1 = this.buildMyData(this.snowflake.nextId(), "luoNew-1");
        MyData myData2 = this.buildMyData(this.snowflake.nextId(), "luoNew-2");
        Integer retCount = this.myDataService.addBothData(myData1, myData2);
        log.info("addBothData retCount: {}", retCount);
    }

    private MyData buildMyData(Long id, String myName) {
        MyData myData = new MyData();
        myData.setId(id);
        myData.setMyName(myName);
        myData.setMyType((byte) 2);
        myData.setMyVersion(0);
        myData.setTenantId(1L);
        myData.setCreatedBy("luo");
        myData.setCreatedTime(LocalDateTime.now());
        myData.setModifiedBy(myData.getCreatedBy());
        myData.setModifiedTime(myData.getCreatedTime());
        return myData;
    }
}
