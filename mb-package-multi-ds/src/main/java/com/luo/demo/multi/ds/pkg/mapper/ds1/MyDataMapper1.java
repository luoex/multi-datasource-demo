package com.luo.demo.multi.ds.pkg.mapper.ds1;

import com.luo.demo.multi.ds.pkg.entity.MyData;

/**
 * <p>
 * 我的数据 Mapper 接口
 * </p>
 *
 * @author luohq
 * @since 2022-08-06
 */
public interface MyDataMapper1 {

    MyData selectById(Long id);

    Integer insert(MyData myData);

}
