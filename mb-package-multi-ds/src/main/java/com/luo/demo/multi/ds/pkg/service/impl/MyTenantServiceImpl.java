package com.luo.demo.multi.ds.pkg.service.impl;

import com.luo.demo.multi.ds.pkg.service.IMyTenantService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 我的租户 服务实现类
 * </p>
 *
 * @author luohq
 * @since 2022-08-06
 */
@Service
public class MyTenantServiceImpl implements IMyTenantService {

}
