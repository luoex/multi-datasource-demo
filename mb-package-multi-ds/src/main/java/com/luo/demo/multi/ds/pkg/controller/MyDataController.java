package com.luo.demo.multi.ds.pkg.controller;

import com.luo.demo.multi.ds.pkg.entity.MyData;
import com.luo.demo.multi.ds.pkg.service.IMyDataService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 * 我的数据 前端控制器
 * </p>
 *
 * @author luohq
 * @since 2022-08-06
 */
@RestController
@RequestMapping("/myData")
@Slf4j
public class MyDataController {

    @Resource
    private IMyDataService myDataService;

    @GetMapping("/ds1/{id}")
    public MyData findByIdFromDs1(@PathVariable Long id) {
        log.info("findByIdFromDs1, id: {}", id);
        MyData myData = this.myDataService.findByIdFromDs1(id);
        log.info("findByIdFromDs1, result: {}", myData);
        return myData;
    }

    @GetMapping("/ds2/{id}")
    public MyData findByIdFromDs2(@PathVariable Long id) {
        log.info("findByIdFromDs2, id: {}", id);
        MyData myData = this.myDataService.findByIdFromDs2(id);
        log.info("findByIdFromDs2, result: {}", myData);
        return myData;
    }
}
