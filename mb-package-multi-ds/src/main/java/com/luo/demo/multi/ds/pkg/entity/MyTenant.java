package com.luo.demo.multi.ds.pkg.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 我的租户
 * </p>
 *
 * @author luohq
 * @since 2022-08-06
 */
@Data
public class MyTenant implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键ID
     */
    private Long id;

    /**
     * 租户名称
     */
    private String tenantName;

    /**
     * 租户详情
     */
    private String tenantDesc;

    /**
     * 版本号
     */
    private Integer myVersion;

    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime createdTime;

    /**
     * 创建人
     */
    private String createdBy;

    /**
     * 修改时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private LocalDateTime modifiedTime;

    /**
     * 修改人
     */
    private String modifiedBy;
}
