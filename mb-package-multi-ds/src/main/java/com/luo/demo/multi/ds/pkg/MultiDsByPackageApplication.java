package com.luo.demo.multi.ds.pkg;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 *
 * @author luohq
 * @date 2022-08-06
 */
@SpringBootApplication
public class MultiDsByPackageApplication {

	public static void main(String[] args) {
		SpringApplication.run(MultiDsByPackageApplication.class, args);
	}

}
