package com.luo.demo.dynamic.tenant.service;

import com.luo.demo.dynamic.tenant.base.BaseTest;
import com.luo.demo.dynamic.tenant.entity.MyTenant;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import javax.annotation.Resource;

/**
 * MyDataService测试
 *
 * @author luohq
 * @date 2022-08-07 08:02
 */
@Slf4j
public class MyTenantServiceTest extends BaseTest {

    @Resource
    private IMyTenantService myTenantService;


    @ParameterizedTest
    @ValueSource(longs = {1L, 2L})
    void findByIdFromDs1(Long id) {
        MyTenant myTenant = this.myTenantService.getById(id);
        log.info("get tenant by id={}, result: {}", myTenant);
        Assertions.assertNotNull(myTenant);
        Assertions.assertEquals(id, myTenant.getId());
    }
}
