package com.luo.demo.dynamic.tenant.service;

import com.luo.demo.dynamic.tenant.base.BaseTest;
import com.luo.demo.dynamic.tenant.entity.MyData;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ActiveProfiles;

import javax.annotation.Resource;
import java.time.LocalDateTime;

/**
 * MyDataMultiDsLocalTxService测试
 *
 * @author luohq
 * @date 2022-08-07 08:02
 */
@Slf4j
//启用application-multi-ds-tx.yml配置
@ActiveProfiles("multi-ds-tx")
public class MyDataMultiDsLocalTxServiceTest extends BaseTest {

    @Resource
    private IMyDataMultiDsLocalTxService myDataMultiDsLocalTxService;
    @Resource
    private IMyDataService myDataService;

    @Test
    void addData1() {
        String myName = "luoNew-1";
        MyData myData = this.buildMyData(myName);
        Integer retCount = this.myDataService.addData1(myData);
        log.info("addData1 retCount: {}", retCount);
    }

    @Test
    void addData2() {
        String myName = "luoNew-2";
        MyData myData = this.buildMyData(myName);
        Integer retCount = this.myDataService.addData2(myData);
        log.info("addData2 retCount: {}", retCount);
    }

    @Test
    void addBothData() {
        String myName1 = "luoNew-11", myName2 = "luoNew-22";
        MyData myData1 = this.buildMyData(myName1);
        MyData myData2 = this.buildMyData(myName2);
        Integer retCount = this.myDataMultiDsLocalTxService.addBothData(myData1, myData2);
        log.info("addBothData retCount: {}", retCount);
    }


    private MyData buildMyData(String myName) {
        MyData myData = new MyData();
        myData.setMyName(myName);
        myData.setMyType((byte) 2);
        //myData.setMyVersion(0);
        myData.setTenantId(1L);
        myData.setCreatedBy("luo");
        myData.setCreatedTime(LocalDateTime.now());
        myData.setModifiedBy(myData.getCreatedBy());
        myData.setModifiedTime(myData.getCreatedTime());
        return myData;
    }
}
