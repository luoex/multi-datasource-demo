package com.luo.demo.dynamic.tenant.controller;

import com.luo.demo.dynamic.tenant.base.BaseTest;
import com.luo.demo.dynamic.tenant.context.TenantContext;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * MyDataController测试
 *
 * @author luohq
 * @date 2022-08-06 15:43
 */
public class MyDataControllerTest extends BaseTest {

    @ParameterizedTest
    @CsvSource(value = {"1,luo", "2,luo-2", "3,luo"}, nullValues = "NIL")
    void findById(String tenantIdHeaderVal, String myName) throws Exception {
        Long id = 1L;
        this.mockMvc.perform(get("/myData/{id}", id)
                //设置租户ID请求头
                .header(TenantContext.TENANT_ID_HEADER, tenantIdHeaderVal))
                //打印请求详细信息
                .andDo(print())
                //判断返回结果200
                .andExpect(status().isOk())
                //判断content-type兼容application/json
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                //判断响应体respCode为成功
                .andExpect(jsonPath("$.id").value(id))
                .andExpect(jsonPath("$.myName").value(myName));
    }
}
