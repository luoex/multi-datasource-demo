package com.luo.demo.dynamic.tenant.service;

import com.luo.demo.dynamic.tenant.base.BaseTest;
import com.luo.demo.dynamic.tenant.context.TenantContext;
import com.luo.demo.dynamic.tenant.dto.MyDataQueryDto;
import com.luo.demo.dynamic.tenant.entity.MyData;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

import javax.annotation.Resource;
import java.time.LocalDateTime;

/**
 * MyDataService测试
 *
 * @author luohq
 * @date 2022-08-07 08:02
 */
@Slf4j
public class MyDataServiceTest extends BaseTest {

    @Resource
    private IMyDataService myDataService;
    @Resource
    private ITenantDsService tenantDsService;

    @BeforeEach
    void clearDsContext() {
        this.tenantDsService.clearDsContext();
    }


    @ParameterizedTest
    @CsvSource(value = {
            //查询DB并切换数据源1
            "1,luo",
            //直接切换数据源1（由于支持已经查询过DB并且动态添加过同名数据源，顾已存在则不必再次查询DB）
            "1,luo",
            //查询DB并切换数据源1
            "2,luo-2",
            //查询DB不存在，不设置数据源则使用默认数据源
            "3,luo",
            //租户ID为空，不设置数据源则使用默认数据源
            "NIL,luo"}, nullValues = "NIL")
    void findById(String tenantId, String myName) {
        Long id = 1L;

        //根据租户ID切换数据源、设置租户上下文
        this.tenantDsService.changeDsByTenantId(tenantId);

        //查询数据
        MyData myData = this.myDataService.findById(id);
        log.info("findByIdFromDs1 result: {}", myData);
        Assertions.assertNotNull(myData);
        Assertions.assertEquals(myName, myData.getMyName());
    }

    @ParameterizedTest
    @CsvSource(value = {
            //查询DB并切换数据源1
            "1,luo",
            //直接切换数据源1（由于支持已经查询过DB并且动态添加过同名数据源，顾已存在则不必再次查询DB）
            "1,luo",
            //查询DB并切换数据源1
            "2,luo-2",
            //查询DB不存在，不设置数据源则使用默认数据源
            "3,luo",
            //租户ID为空，不设置数据源则使用默认数据源
            "NIL,luo"}, nullValues = "NIL")
    void findByQuery(String tenantId, String myName) {
        Long id = 1L;

        //根据租户ID切换数据源、设置租户上下文
        this.tenantDsService.changeDsByTenantId(tenantId);

        MyDataQueryDto myDataQueryDto = MyDataQueryDto.builder()
                .id(id)
                .myName(myName)
                .build();
        MyData myData = this.myDataService.findByQuery(myDataQueryDto);
        log.info("findByQuery result: {}", myData);
        Assertions.assertNotNull(myData);
        Assertions.assertEquals(myName, myData.getMyName());
    }

    @ParameterizedTest
    @CsvSource(value = {
            //查询DB并切换数据源1
            "1,luo",
            //直接切换数据源1（由于支持已经查询过DB并且动态添加过同名数据源，顾已存在则不必再次查询DB）
            "1,luo",
            //查询DB并切换数据源1
            "2,luo-2",
            //查询DB不存在，不设置数据源则使用默认数据源
            "3,luo",
            //租户ID为空，不设置数据源则使用默认数据源
            "NIL,luo"}, nullValues = "NIL")
    void findByName(String tenantId, String myName) {
        //根据租户ID切换数据源、设置租户上下文
        this.tenantDsService.changeDsByTenantId(tenantId);

        MyData myData = this.myDataService.findByName(myName);
        log.info("findByName result: {}", myData);
        Assertions.assertNotNull(myData);
        Assertions.assertEquals(myName, myData.getMyName());
    }

    @ParameterizedTest
    @CsvSource(value = {
            //查询DB并切换数据源1
            "1,luoNew-1",
            //直接切换数据源1（由于支持已经查询过DB并且动态添加过同名数据源，顾已存在则不必再次查询DB）
            "1,luoNew-11",
            //查询DB并切换数据源1
            "2,luoNew-2",
            //查询DB不存在，不设置数据源则使用默认数据源
            "3,luoNew-111",
            //租户ID为空，不设置数据源则使用默认数据源
            "NIL,luoNew-1111"}, nullValues = "NIL")
    void addData(String tenantId, String myName) {
        //根据租户ID切换数据源、设置租户上下文
        this.tenantDsService.changeDsByTenantId(tenantId);

        MyData myData = this.buildMyData(myName, TenantContext.getTenant());
        Integer retCount = this.myDataService.addData(myData);
        log.info("addData retCount: {}", retCount);
    }


    private MyData buildMyData(String myName, String tenantId) {
        MyData myData = new MyData();
        myData.setMyName(myName);
        myData.setMyType((byte) 2);
        //myData.setMyVersion(0);
        myData.setTenantId(Long.valueOf(tenantId));
        myData.setCreatedBy("luo");
        myData.setCreatedTime(LocalDateTime.now());
        myData.setModifiedBy(myData.getCreatedBy());
        myData.setModifiedTime(myData.getCreatedTime());
        return myData;
    }
}
