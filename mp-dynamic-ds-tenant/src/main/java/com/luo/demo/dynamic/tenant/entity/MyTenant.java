package com.luo.demo.dynamic.tenant.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

/**
 * <p>
 * 我的租户
 * </p>
 *
 * @author luohq
 * @since 2022-08-08
 */
@Data
@ToString(callSuper = true)
@EqualsAndHashCode(callSuper = true)
@TableName("my_tenant")
public class MyTenant extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 租户名称
     */
    private String tenantName;

    /**
     * 租户详情
     */
    private String tenantDesc;

    /**
     * 租户数据库URL
     */
    private String dbUrl;

    /**
     * 租户数据库用户名
     */
    private String dbUsername;

    /**
     * 租户数据库密码
     */
    private String dbPassword;

    /**
     * 租户数据库驱动类
     */
    private String dbDriverClassName;

    /**
     * 版本号
     */
    @Version
    private Integer myVersion;
}
