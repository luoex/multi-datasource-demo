package com.luo.demo.dynamic.tenant.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.Version;
import lombok.*;

/**
 * <p>
 * 我的数据
 * </p>
 *
 * @author luohq
 * @since 2022-08-08
 */
@Data
@ToString(callSuper = true)
//@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true)
//@EqualsAndHashCode(callSuper = true, onlyExplicitlyIncluded = true, of = {"myUinqColumn1", "myUniquColum2"})
@EqualsAndHashCode(callSuper = true)
@TableName("my_data")
public class MyData extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 名称
     */
    private String myName;

    /**
     * 类型
     */
    private Byte myType;

    /**
     * 版本号
     */
    @Version
    private Integer myVersion;

    /**
     * 租户ID
     */
    private Long tenantId;
}
