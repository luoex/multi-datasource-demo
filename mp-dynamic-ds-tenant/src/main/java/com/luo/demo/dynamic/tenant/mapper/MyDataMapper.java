package com.luo.demo.dynamic.tenant.mapper;

import com.luo.demo.dynamic.tenant.entity.MyData;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 我的数据 Mapper 接口
 * </p>
 *
 * @author luohq
 * @since 2022-08-08
 */
public interface MyDataMapper extends BaseMapper<MyData> {

    MyData selectByName(String myName);

}
