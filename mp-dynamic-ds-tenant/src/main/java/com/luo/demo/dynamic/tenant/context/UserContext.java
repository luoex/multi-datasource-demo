package com.luo.demo.dynamic.tenant.context;

/**
 * 用户上下文
 *
 * @author luohq
 * @version 2022-08-08
 */
public class UserContext {

    public static String USER_ID_HEADER = "X-USER-INFO";

    private static ThreadLocal<String> userLocal = ThreadLocal.withInitial(() -> "luo");

    public UserContext() {
    }

    public static String getUser() {
        return userLocal.get();
    }

    public static void setUser(String user) {
        userLocal.set(user);
    }

    public static void remove() {
        userLocal.remove();
    }
}