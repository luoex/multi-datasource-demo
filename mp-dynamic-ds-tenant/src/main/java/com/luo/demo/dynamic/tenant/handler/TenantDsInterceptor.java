package com.luo.demo.dynamic.tenant.handler;

import com.luo.demo.dynamic.tenant.context.TenantContext;
import com.luo.demo.dynamic.tenant.service.ITenantDsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 租户切换数据源拦截器
 *
 * @author luohq
 * @date 2022-08-08
 */
@Slf4j
@Component
public class TenantDsInterceptor implements HandlerInterceptor {

    @Resource
    private ITenantDsService tenantDsService;

    /**
     * 在请求处理之前进行调用（Controller方法调用之前）
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        String requestURI = request.getRequestURI();
        log.info("经过多数据源Interceptor,当前路径是{}", requestURI);
        //String headerDs = request.getHeader("ds");
        //Object sessionDs = request.getSession().getAttribute("ds");
        String tenantId = request.getHeader(TenantContext.TENANT_ID_HEADER);
        //若tenantId为空，则使用默认数据源
        if (!StringUtils.hasText(tenantId)) {
            log.warn("cur request tenant id header val is null!");
            tenantId = TenantContext.DEFAULT_TENANT_ID;
        }
        //根据tenantId切换数据源
        this.tenantDsService.changeDsByTenantId(tenantId);
        return true;
    }

    /**
     * 请求处理之后进行调用，但是在视图被渲染之前（Controller方法调用之后）
     */
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) {

    }

    /**
     * 在整个请求结束之后被调用，也就是在DispatcherServlet 渲染了对应的视图之后执行（主要是用于进行资源清理工作）
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
        //清空当前线程数据源
        this.tenantDsService.clearDsContext();
    }

}