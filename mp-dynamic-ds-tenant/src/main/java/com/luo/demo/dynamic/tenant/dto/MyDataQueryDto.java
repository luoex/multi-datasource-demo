package com.luo.demo.dynamic.tenant.dto;

import lombok.Builder;
import lombok.Data;

/**
 * MyData查询参数 DTO
 *
 * @author luohq
 * @date 2022-08-07 12:41
 */
@Data
@Builder
public class MyDataQueryDto {
    /**
     * 主键ID
     */
    private Long id;

    /**
     * 名称
     */
    private String myName;
}
