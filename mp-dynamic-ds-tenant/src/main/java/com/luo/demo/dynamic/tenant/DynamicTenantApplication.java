package com.luo.demo.dynamic.tenant;

import com.luo.demo.dynamic.tenant.mapper.MyDataMapper;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 启动类
 *
 * @author luohq
 * @date 2022-08-06
 */
@SpringBootApplication
@MapperScan(basePackageClasses = {MyDataMapper.class})
public class DynamicTenantApplication {

	public static void main(String[] args) {
		SpringApplication.run(DynamicTenantApplication.class, args);
	}

}
