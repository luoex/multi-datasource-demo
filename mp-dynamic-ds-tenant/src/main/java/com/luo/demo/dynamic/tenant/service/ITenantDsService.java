package com.luo.demo.dynamic.tenant.service;

/**
 * 租户切换数据源 - 接口定义
 *
 * @author luohq
 * @date 2022-08-08 16:53
 */
public interface ITenantDsService {
    /**
     * 根据租户ID切换数据源
     *
     * @param tenantId 租户ID
     */
    void changeDsByTenantId(String tenantId);

    /**
     * 当前应用是否已在内存中加载过此数据源
     *
     * @param dsName
     * @return
     */
    Boolean existDsInMemory(String dsName);

    /**
     * 清理当前调用上下文中的数据源缓存
     */
    void clearDsContext();

    /**
     * 移除对应的数据源信息
     *
     * @param dsName 数据源名称
     */
    void removeDs(String dsName);
}
