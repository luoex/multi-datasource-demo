package com.luo.demo.dynamic.tenant.handler;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.luo.demo.dynamic.tenant.context.UserContext;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;

/**
 * 自动填充字段处理器 - 实现类
 *
 * @author luohq
 * @date 2022-08-08
 */
@Slf4j
@Component
public class MyMetaObjectHandler implements MetaObjectHandler {

    @Override
    public void insertFill(MetaObject metaObject) {
        log.debug("start insert fill ....");
        this.strictInsertFill(metaObject, "createdTime", LocalDateTime.class, LocalDateTime.now());
        this.strictInsertFill(metaObject, "createdBy", String.class, UserContext.getUser());
        this.strictInsertFill(metaObject, "modifiedTime", LocalDateTime.class, LocalDateTime.now());
        this.strictInsertFill(metaObject, "modifiedBy", String.class, UserContext.getUser());
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        log.debug("start update fill ....");
        this.strictUpdateFill(metaObject, "modifiedTime", LocalDateTime.class, LocalDateTime.now());
        this.strictUpdateFill(metaObject, "modifiedBy", String.class, UserContext.getUser());
    }
}