package com.luo.demo.dynamic.tenant.service;

import com.luo.demo.dynamic.tenant.dto.MyDataQueryDto;
import com.luo.demo.dynamic.tenant.entity.MyData;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 我的数据 服务类
 * </p>
 *
 * @author luohq
 * @since 2022-08-08
 */
public interface IMyDataService extends IService<MyData> {

    MyData findById(Long id);
    MyData findByQuery(MyDataQueryDto myDataQueryDto);
    MyData findByName(String myName);
    Integer addData(MyData myData);
    Integer addData1(MyData myData);
    Integer addData2(MyData myData);
}
