package com.luo.demo.dynamic.tenant.mapper;

import com.luo.demo.dynamic.tenant.entity.MyTenant;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 我的租户 Mapper 接口
 * </p>
 *
 * @author luohq
 * @since 2022-08-08
 */
public interface MyTenantMapper extends BaseMapper<MyTenant> {

}
