package com.luo.demo.dynamic.tenant.service.impl;

import com.luo.demo.dynamic.tenant.entity.MyTenant;
import com.luo.demo.dynamic.tenant.mapper.MyTenantMapper;
import com.luo.demo.dynamic.tenant.service.IMyTenantService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 我的租户 服务实现类
 * </p>
 *
 * @author luohq
 * @since 2022-08-08
 */
@Service
public class MyTenantServiceImpl extends ServiceImpl<MyTenantMapper, MyTenant> implements IMyTenantService {

}
