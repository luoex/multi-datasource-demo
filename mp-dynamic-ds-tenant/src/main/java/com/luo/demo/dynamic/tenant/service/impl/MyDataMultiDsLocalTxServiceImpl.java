package com.luo.demo.dynamic.tenant.service.impl;

import com.baomidou.dynamic.datasource.annotation.DS;
import com.baomidou.dynamic.datasource.annotation.DSTransactional;
import com.luo.demo.dynamic.tenant.entity.MyData;
import com.luo.demo.dynamic.tenant.service.IMyDataMultiDsLocalTxService;
import com.luo.demo.dynamic.tenant.service.IMyDataService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 本地多数据源事务 - 测试服务实现类<br/>
 * 注：测试该类需开启spring.profiles.active=multi-ds-tx
 *
 * @author luohq
 * @date 2022-08-09 13:42
 */
@Service
public class MyDataMultiDsLocalTxServiceImpl implements IMyDataMultiDsLocalTxService {

    @Resource
    private IMyDataService myDataService;

    /**
     * 此处需使用@DSTransactional，需注意不是Spring @Transactional，
     * 使用@DSTransactional支持切换数据源，而@Transactional方法中无法切换数据源
     * 注：需跨服务调用切换DS，否则仅使用第一个数据源，即2条记录都插入到ds1中
     * <p>
     * | 主服务 | ds1服务 | ds2服务 | 效果 |
     * |:-----|:-------|:-------|:------|
     * | @DSTransactional | @DSTransactional | @DSTransactional | 调用主服务支持全局事务提交、回滚，单独调用ds服务各自支持事务 |
     * | @DSTransactional | @Transactional | @Transactional | 调用主服务支持全局事务提交、回滚，单独调用ds服务各自支持事务 |
     * | @DSTransactional | 无 | 无 | 调用主服务支持全局事务提交、回滚，单独调用ds服务不支持事务 |
     * | 无 | @DSTransactional | @DSTransactional | 调用ds服务各自管理自身事务，不支持事务全局回滚 |
     * | @Transactional | | | Spring @Transactional不支持切换数据源 |
     */
    @Override
    @DSTransactional
    //@DS("ds1") //如果ds1是默认数据源则不需要DS注解。
    public Integer addBothData(MyData myData1, MyData myData2) {
        Integer retCount1 = this.myDataService.addData1(myData1);
        Integer retCount2 = this.myDataService.addData2(myData2);
        //if (true) {
        //    throw new RuntimeException("测试多数据源异常回滚！");
        //}
        return retCount1 + retCount2;
    }

}
