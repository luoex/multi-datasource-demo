package com.luo.demo.dynamic.tenant.service;

import com.luo.demo.dynamic.tenant.entity.MyTenant;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 我的租户 服务类
 * </p>
 *
 * @author luohq
 * @since 2022-08-08
 */
public interface IMyTenantService extends IService<MyTenant> {

}
