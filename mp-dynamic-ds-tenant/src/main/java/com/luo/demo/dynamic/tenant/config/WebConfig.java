package com.luo.demo.dynamic.tenant.config;

import com.luo.demo.dynamic.tenant.handler.TenantDsInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;

/**
 * web相关配置
 *
 * @author luohq
 * @date 2021-12-24 12:38
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {

    @Resource
    private TenantDsInterceptor tenantDsInterceptor;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //注册租户切换数据源拦截器
        registry.addInterceptor(this.tenantDsInterceptor);
    }
}