package com.luo.demo.dynamic.tenant.controller;

import com.luo.demo.dynamic.tenant.context.TenantContext;
import com.luo.demo.dynamic.tenant.entity.MyData;
import com.luo.demo.dynamic.tenant.service.IMyDataService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * <p>
 * 我的数据 前端控制器
 * </p>
 *
 * @author luohq
 * @since 2022-08-08
 */
@RestController
@RequestMapping("/myData")
@Slf4j
public class MyDataController {

    @Resource
    private IMyDataService myDataService;

    @GetMapping("/{id}")
    public MyData findById(@PathVariable Long id) {
        log.info("findById, id: {}, tenantId: {}", id, TenantContext.getTenant());
        MyData myData = this.myDataService.findById(id);
        log.info("findById, result: {}", myData);
        return myData;
    }
}
