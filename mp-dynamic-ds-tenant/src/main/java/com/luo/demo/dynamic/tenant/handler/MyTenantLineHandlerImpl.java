package com.luo.demo.dynamic.tenant.handler;

import com.baomidou.mybatisplus.extension.plugins.handler.TenantLineHandler;
import com.luo.demo.dynamic.tenant.context.TenantContext;
import lombok.extern.slf4j.Slf4j;
import net.sf.jsqlparser.expression.Expression;
import net.sf.jsqlparser.expression.LongValue;

import java.util.Arrays;
import java.util.List;

/**
 * 租户处理器
 * 注：本多租户示例暂未使用此Mybatis-Plus逻辑多租户实现
 *
 * @author luohq
 * @date 2022-08-07 12:31
 */
@Slf4j
public class MyTenantLineHandlerImpl implements TenantLineHandler {

    private static final List<String> IGNORE_TABLES = Arrays.asList("my_tenant");

    @Override
    public Expression getTenantId() {
        return new LongValue(TenantContext.getTenant());
    }

    @Override
    public String getTenantIdColumn() {
        //默认tenant_id
        return "tenant_id";
    }

    @Override
    public boolean ignoreTable(String tableName) {
        return IGNORE_TABLES.contains(tableName);
    }
}