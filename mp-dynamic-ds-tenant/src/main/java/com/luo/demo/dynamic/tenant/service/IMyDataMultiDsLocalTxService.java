package com.luo.demo.dynamic.tenant.service;

import com.luo.demo.dynamic.tenant.entity.MyData;

/**
 * 本地多数据源事务 - 测试服务接口
 *
 * @author luohq
 * @date 2022-08-09 13:39
 */
public interface IMyDataMultiDsLocalTxService {
    Integer addBothData(MyData myData1, MyData myData2);
}
