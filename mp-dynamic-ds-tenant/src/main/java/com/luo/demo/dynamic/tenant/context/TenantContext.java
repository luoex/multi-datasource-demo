package com.luo.demo.dynamic.tenant.context;

/**
 * 租户上下文
 *
 * @author luohq
 * @version 2022-08-08
 */
public class TenantContext {

    public static String TENANT_ID_HEADER = "X-TENANT-ID";
    public static String DEFAULT_TENANT_ID = "1";

    private static ThreadLocal<String> tenantLocal = ThreadLocal.withInitial(() -> DEFAULT_TENANT_ID);

    public TenantContext() {
    }

    public static String getTenant() {
        return tenantLocal.get();
    }

    public static void setTenant(String tenant) {
        tenantLocal.set(tenant);
    }

    public static void remove() {
        tenantLocal.remove();
    }
}