package com.luo.demo.mp.generator;

import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.Collections;

/**
 * Mybatis-Plus代码生成器（新）
 *
 * @author luohq
 * @date 2022-08-06 09:56
 */
public class MyMpGenerator {
    //作者名称
    private static final String AUTHOR = "luohq";
    //mysql数据源连接配置
    private static final String DATA_SOURCE_DRIVER_NAME = "com.mysql.cj.jdbc.Driver";
    private static final String DATA_SOURCE_URL = "jdbc:mysql://localhost:3306/multi-ds-1?serverTimezone=Asia/Shanghai&useUnicode=true&characterEncoding=utf-8&allowMultiQueries=true&nullCatalogMeansCurrent=true";
    private static final String DATA_SOURCE_USER_NAME = "root";
    private static final String DATA_SOURCE_PASSWORD = "123456";
    //所包含的tables（为空则表示生成当前DB下的所有table）
    private static final String[] DATA_SOURCE_INCLUDE_TABLES = new String[] {};
    //java代码包名
    private static final String PACKAGE_NAME = "com.luo.demo.dynamic.tenant";
    private static final String MODULE = "mp-dynamic-ds-tenant";
    private static final String MODULE_DIR = "/mp-dynamic-ds-tenant";
    //java基础类
    private static final String ENTITY_SUPER_CLASS = "com.luo.demo.dynamic.tenant.entity.BaseEntity";

    //java代码生成路径
    private static final String JAVA_OUTPUT_DIR = System.getProperty("user.dir") + MODULE_DIR + "/src/main/java";
    //mapper.xml生成路径
    private static final String MAPPER_XML_OUTPUT_DIR = System.getProperty("user.dir") + MODULE_DIR + "/src/main/resources/mapper";



    public static void main(String[] args) {
        FastAutoGenerator.create(DATA_SOURCE_URL, DATA_SOURCE_USER_NAME, DATA_SOURCE_PASSWORD)
                .globalConfig(builder -> {
                    builder.author(AUTHOR) // 设置作者
                            //.enableSwagger() // 开启 swagger 模式
                            .dateType(DateType.TIME_PACK) //日期类型
                            .outputDir(JAVA_OUTPUT_DIR); // 指定输出目录
                })
                .packageConfig(builder -> {
                    builder.parent(PACKAGE_NAME) // 设置父包名
                            //.moduleName("system") // 设置父包模块名
                            .pathInfo(Collections.singletonMap(OutputFile.xml, MAPPER_XML_OUTPUT_DIR)); // 设置mapperXml生成路径
                })
                .strategyConfig(builder -> {
                    builder
                            .entityBuilder()
                                //启用lombok
                                .enableLombok()
                                //设置乐观锁字段@Version
                                .versionColumnName("my_version")
                                .versionPropertyName("myVersion")
                                //设置父Entity
                                .superClass(ENTITY_SUPER_CLASS)
                                //设置父类公共字段
                                .addSuperEntityColumns("id", "created_time", "created_by", "modified_time", "modified_by")
                                .enableFileOverride()
                            .mapperBuilder()
                                .enableBaseColumnList()
                                .enableBaseResultMap()
                                .enableFileOverride()
                            .serviceBuilder()
                                .enableFileOverride()
                                ////设置空父Service
                                //.superServiceClass((String) null)
                                //.superServiceImplClass((String) null)
                            .controllerBuilder()
                                .enableRestStyle()
                                .enableFileOverride()
                            ;
                    //builder.addInclude("t_simple") // 设置需要生成的表名
                    //        .addTablePrefix("t_", "c_"); // 设置过滤表前缀
                })
                .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .execute();

    }
}
